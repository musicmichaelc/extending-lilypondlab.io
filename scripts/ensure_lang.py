"""Check that the language given on the command line belongs to the `LINGUAS` file.

If it does, perform a few configuration actions to make sure that language
is set up. This makes adding a new language as easy as adding an entry
to `LINGUAS`.
"""

from argparse import ArgumentParser
from pathlib import Path

p = ArgumentParser()
p.add_argument("lang")
args = p.parse_args()

langs = Path("LINGUAS").read_text(encoding="utf-8").splitlines()

if args.lang not in langs:
    raise SystemExit(
        f"Language {args.lang} does not exist. Fix spelling, or if you intended to create a new translation, add it to the LINGUAS file first."
    )

if args.lang != "en":
    lang_path = Path(args.lang)
    lang_path.mkdir(exist_ok=True)

    locales_lang_path = Path("src") / "locales" / args.lang
    locales_lang_path.mkdir(exist_ok=True)
    lc_messages_path = locales_lang_path / "LC_MESSAGES"
    if not lc_messages_path.is_symlink():
        lc_messages_path.symlink_to(Path("..") / ".." / ".." / args.lang)
