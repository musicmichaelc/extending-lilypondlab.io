# Extending LilyPond

This is an unofficial resource on extending LilyPond in Scheme.

You will find:

- A Scheme tutorial, for learning the language itself, with an eye on using it
  in LilyPond.

- A complete guide to extending. It assumes Scheme knowledge (which can be
  acquired through the tutorial or other resources), and aims at covering how
  LilyPond works internally and how to extend these inner workings to suit your
  needs.

Questions about Scheme and LilyPond are welcome on the
[lilypond-user mailing list](https://lists.gnu.org/mailman/listinfo/lilypond-user).
If you find any inaccuracies in this document, you want to suggest an
improvement, or you would like to help with translating it into other languages,
please read [](about).

This document is free to reuse. It is placed under the
[Creative Commons CC0 license](https://creativecommons.org/publicdomain/zero/1.0/).

```{toctree}
---
maxdepth: 1
---
why-scheme
scheme/index
extending/index
about
```
