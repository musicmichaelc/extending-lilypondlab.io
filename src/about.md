```{highlight} shell
```

# About this document

## Contributing

Sources for this document are hosted on GitLab in the repository
https://gitlab.com/extending-lilypond/extending-lilypond.gitlab.io.

If you wish to report an error or suggest an improvement, please either
[open an issue on GitLab](https://gitlab.com/extending-lilypond/extending-lilypond.gitlab.io/-/issues)
or [contact me directly](https://jean.abou-samra.fr). You can also use the "Edit
this page" button in the right sidebar, which should take you to an easy-to-use
interface for proposing simple changes.

## Building

Sources are in [MyST Markdown](https://myst-parser.readthedocs.io) and rendered
with [Sphinx](https://www.sphinx-doc.org).

You need Python and [Hatch](https://hatch.pypa.io). Other dependencies are taken
care of automatically by Hatch. To build the guide, run the command `hatch run
build en`. Then open `public/en/index.html` in your browser to view the result.

The guide contains LilyPond snippets. The previous command will *not* compile
these (to keep the build fast). If you want to compile them, you need to run
`hatch run lilypond en`.

## Translating

Translations are done in PO files, which associate a paragraph of English text
with a translated paragraph. There are currently three translations, one into
French, and one into German which is a work-in-progress. You are welcome to add
more languages (but accept this friendly tip: do this if you have a good
understanding of this document in the first place).

In the following, replace `LANG` with the language code of your language (e.g.,
`fr`, `de`, etc.).

Before you start working on the translation, run

```
$ hatch run update LANG
```

to bring the PO files up to date with the current English text. This adds,
removes and obsoletes entries from the PO files.

To view the translation work remaining, use

```
$ hatch run todo LANG
```

To build the documentation in your language, run

```
$ hatch run build LANG
```

and open `public/LANG/index.html`.

The outputs of LilyPond snippets are shared between languages, so if you have
run `hatch run lilypond en`, snippets that you have not changed in the
translation will have images. However, if you translated some snippets, you also
need to run `hatch run lilypond LANG` in order to compile the translated
snippets.

To add a new translation, first add the language name (i.e., `LANG`) at the
bottom of the file `LINGUAS`. Afterwards, running `hatch run update LANG` for
the first time will create a folder `LANG/` where you will find the PO files to
translate.

Code blocks are also registered in PO files and can be translated if you wish.
Parts you may want to translate include comments and variable names. This is
especially true in the Scheme tutorial. The extending guide, on the other hand,
has a few somewhat bulky code examples; it's not recommended to translate those,
since updating them afterwards will be cumbersome. In those cases, you can just
set the translation to the original. (You can also leave the translation empty,
in which case the original is used as well, but it is preferable to copy the
original as the translation, as this allows the entry not to be taken into
account in `hatch run todo LANG`.)

You can also reformat `.po` files. It is recommended to do this before
committing so that future updates will not reformat the parts you've already
translated.

```
$ hatch run pofmt LANG
```

## Link check

To check for broken hyperlinks, use

```
$ hatch run linkcheck en
```

or (for translations)

```
$ hatch run linkcheck LANG
```
